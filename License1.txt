Permission is granted to copy, use and modify (but not 
redistribute) the files in this folder for academic / 
educational purpose only.

The distribution of these files on dedicated servers or
by other media requires written permission from info@pitschel.pw

Copying, usage or modification for commercial purpose
requires written permission from info@pitschel.pw.

Inclusion of this source code (or parts thereof) into
software packages consequently requires written permission 
from info@pitschel.pw.


