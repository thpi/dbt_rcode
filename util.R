# Changelog:
#   2019-06-28  

# Computes the inverse of matrix A
inv <- function(A) { return (solve(A)); }

# Prints out a value with a caption:
pp <- function(cap_, num_)
{
    if (is.numeric(num_))
    {
        numstr_ = sprintf("%10.9f", num_);
        print((cat(cap_, numstr_, ' ')));
    } else {
        print((cat(cap_, num_, ' ')));
    }
    return (invisible(NULL));
}

# From https://stackoverflow.com/questions/1826519/how-to-assign-from-a-function-which-returns-more-than-one-value
':=' <- function(lhs, rhs) {
  frame <- parent.frame()
  lhs <- as.list(substitute(lhs))
  if (length(lhs) > 1)
    lhs <- lhs[-1]
  if (length(lhs) == 1) {
    do.call(`=`, list(lhs[[1]], rhs), envir=frame)
    return(invisible(NULL)) 
  }
  if (is.function(rhs) || is(rhs, 'formula'))
    rhs <- list(rhs)
  if (length(lhs) > length(rhs))
    rhs <- c(rhs, rep(list(NULL), length(lhs) - length(rhs)))
  for (i in 1:length(lhs))
    do.call(`=`, list(lhs[[i]], rhs[[i]]), envir=frame)
  return(invisible(NULL)) 
}

