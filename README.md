# DBT repository containing R code

This is a proof-of-concept repository containing code for the DBT algorithm [1] in the univariate case.
For a given finitely discrete distribution F_X, the algorithm computes the distribution of a
linear mixture of independent copies of X ~ F_X. 

Checkout the directory, start R, change into the directory and type

~~~
  source("test_dbt.R")
~~~

to see an example run.

**License**

This is for academic or personal use only. See
 License1.txt for details.

Feel free to link to this repository. Please use the relevant paper on arxiv (e.g. [1]) when 
citing the work in an academic paper.

**References**

[1] "Efficient computation of the cumulative distribution function of a linear mixture of independent random variables". June 2019. arXiv:1906.07186

**Screenshot**

The following shows the density of a random variable Z = X_1 + ... + X_5, where the X_i are independently and identically
distributed with a discrete distribution consisting of 20 data points, which in turn were previously drawn uniformly from [0,20].
The data points are plotted as red diamonds below (stretched on x-axis by factor 5 for better display). The blue curve is the 
raw estimate (i.e. before isotonic regression).

![algo result graph](imgs/linear_mixt_dens_03_27_asidetext.png)

(Possibly, bitbucket's markdown fails on the image syntax; find the image in the imgs/ folder instead.)

**Download**

Use the "Downloads" link on the left pane to download the whole repository.

